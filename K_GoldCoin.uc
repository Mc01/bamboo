class K_GoldCoin extends BambooActor;

defaultproperties
{
	Begin Object Class=StaticMeshComponent Name=GoldCoin
		StaticMesh=StaticMesh'BambooGame.Mx.CoinG'		
		
		bCastDynamicShadow=True
		bForceDirectLightMap=False
		bAllowCullDistanceVolume=True
		bAcceptsStaticDecals=False
		bAcceptsDynamicDecals=False
		CastShadow=True
		bAcceptsDynamicDominantLightShadows=True
		bAcceptsLights=True
		bAcceptsDynamicLights=True
		bUsePrecomputedShadows=False
	End Object
	Components.Add(GoldCoin)
	ActorMesh=GoldCoin
	
	Money=10
}