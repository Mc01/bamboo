//KActor Instance
class BambooActor extends Actor;

//Vars
	var int Money, Income, IsDeadAlready, XGravity;
	var float MoveTime;
	var vector MoveVector, StartVector, StopVector;
	var StaticMeshComponent ActorMesh; 

//Events
simulated event PostBeginPlay()
{
	super.PostBeginPlay();
	
	//Collision related
	IsDeadAlready = 0;
	
	//Money related
	Income = 0;
	ActorMesh.BodyInstance.CustomGravityFactor = 0.0001;
	
	//XMove related
	XGravity = BambooPlay(WorldInfo.Game).XGravity;
	MoveVector = vect(0,0,0);
	MoveVector.Z = XGravity;
	StartVector = Location;
	StopVector = Location + MoveVector;
	//`log("Location.Y: "$Location.Y);
	
	MoveTime = BambooPlay(WorldInfo.Game).MoveTime;
}

simulated event Tick( float DeltaTime )
{
	super.Tick(DeltaTime);
     if((ActorMesh != None))
	 {
		XMove(MoveVector, MoveTime, DeltaTime);
	 }
}

simulated event RigidBodyCollision(PrimitiveComponent HitComponent, PrimitiveComponent OtherComponent, const out CollisionImpactData RigidCollisionData, int ContactIndex)
{
	if((OtherComponent == BambooPlay(WorldInfo.Game).ChestForActor) && (IsDeadAlready == 0))
	{
		IsDeadAlready = 1;
		Income = Money * int(RandRange(1.0,5.0));
		BambooPlay(WorldInfo.Game).PlayerMoney += Income;
		
		if(BambooPlay(WorldInfo.Game).DebugMode == 1){
		`log("HitComponent: "$HitComponent);
		`log("OtherComponent: "$OtherComponent);		
		`log("Income: "$Income);
		`log("Money: "$BambooPlay(WorldInfo.Game).PlayerMoney);
		}
		
		self.SetTimer(BambooPlay(WorldInfo.Game).SleepMonkey,False,'SleepingDeath');
	}
	else if((OtherComponent == None) && (IsDeadAlready == 0))
	{
		IsDeadAlready = 1;
		
		if(BambooPlay(WorldInfo.Game).DebugMode == 1){
		`log("HitComponent: "$HitComponent);
		`log("OtherComponent: "$OtherComponent);
		}
		
		self.SetTimer(BambooPlay(WorldInfo.Game).SleepFloor,False,'SleepingDeath');
	}
}

simulated event SleepingDeath()
{
	self.Destroy();
}

//This version of XMove is only for future development
simulated function XMove(vector XVector, float XTime, float DeltaTime)
{
	local vector DeltaVector;
	local float TimeElapsed;
	
	if((Location == StopVector) || (MoveVector.Z == 0) || (MoveTime <= 0))
	{		
		SetPhysics(PHYS_RigidBody);
		MoveTime = 0;
		MoveVector = vect(0,0,0);
	}
	
	TimeElapsed = XTime / DeltaTime;
	DeltaVector = XVector /  TimeElapsed;
	Move(DeltaVector);
	MoveVector = MoveVector - DeltaVector;
	MoveTime = MoveTime - DeltaTime;
}

defaultproperties
{
	Begin Object Class=StaticMeshComponent Name=K_Collision
		StaticMesh=StaticMesh'BambooGame.Mx.K_Collision0'
		
		HiddenGame=True
		HiddenEditor=True
		
		CollideActors=True
		BlockRigidBody=True
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,BlockingVolume=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		
		bNotifyRigidBodyCollision=True
		ScriptRigidBodyCollisionThreshold=0.001
	End Object
	Components.Add(K_Collision)
	CollisionComponent=K_Collision
	
	CollisionType=COLLIDE_BlockAll 
	bCollideActors=True	
	
	Money=0	
}