//HUD Handler, automated and based on externals
class BambooHUD extends HUD;

//Vars
var BambooGFx GFxMovie;
var BambooGFlite liteMovie; 
var class<BambooGFx> HUDGFx;
var class<BambooGFlite> HUDbegin, HUDpause;

//Events
simulated event PostBeginPlay()
{	
	`log("HeroesHUD PostRender() started!");
	super.PostBeginPlay();
	CreateHUDMovie();
	`log("HeroesHUD PostRender() is amazing!");
}

simulated event PostRender() //PostRender() will tick 10 times in 1 second!
{
	super.PostRender();
	//DrawBeer("Your Money:",20,20,80,80,200);
	//DrawBeer("Your Time:",(Canvas.ClipX)-200,20,80,80,200);

	if(GFxMovie != None)
	{
		GFxMovie.TickHud(0);
	}
	
	if(BambooPlay(WorldInfo.Game).liteFlag == 1) //Begin
	{
		CreateBegin();
		`log("Begin Flag");
		BambooPlay(WorldInfo.Game).liteFlag = 0;
	}
	
	if(BambooPlay(WorldInfo.Game).liteFlag == 2) //Pause
	{
		CreatePause();
		`log("Pause Flag");
		BambooPlay(WorldInfo.Game).liteFlag = 0;
	}
	
	if(BambooPlay(WorldInfo.Game).liteFlag == 3) //Remove
	{
		RemoveLite(); 
		`log("Remove Flag");
		BambooPlay(WorldInfo.Game).liteFlag = 0;
	}
	
	if((BambooPlay(WorldInfo.Game).OpenState == "Arrived") || (BambooPlay(WorldInfo.Game).OpenState == "AfterGame"))
	{
		DrawBeer("Press 'Space' to Start Over",((Canvas.ClipX)/2)-100,((Canvas.ClipY)/2),200,80,80);
	}
}

singular event Destroyed()
{
	RemoveMovies();
	super.Destroyed();
}

//Functions
function RemoveMovies()
{
	if(GFxMovie != None)
	{
		GFxMovie.Close(True);
		GFxMovie = None;
	}
}

function RemoveLite()
{
	if(liteMovie != None)
	{
		liteMovie.Close(True);
		liteMovie = None;
	};
}

function CreateHUDMovie()
{
	GFxMovie = new HUDGFx;
	GFxMovie.SetTimingMode(TM_Real);
	GFxMovie.Init(class'Engine'.static.GetEngine().GamePlayers[GFxMovie.LocalPlayerOwnerIndex]);
}

function CreateBegin()
{
	liteMovie = new HUDbegin;
	liteMovie.SetTimingMode(TM_Real);
	liteMovie.Init(class'Engine'.static.GetEngine().GamePlayers[liteMovie.LocalPlayerOwnerIndex]);
}

function CreatePause()
{
	liteMovie = new HUDpause;
	liteMovie.SetTimingMode(TM_Real);
	liteMovie.Init(class'Engine'.static.GetEngine().GamePlayers[liteMovie.LocalPlayerOwnerIndex]);
}

function DrawBeer(string Nameplate, int X, int Y, int R, int G, int B)
{
	//Lyrics
	Canvas.SetPos(X,Y);
	Canvas.SetDrawColor(R,G,B);
	Canvas.Font = class'Engine'.static.GetSmallFont();
    Canvas.DrawText(Nameplate);
	
	Canvas.SetPos(X+100,Y);
	if(Nameplate == "Your Money:")
	{
		Canvas.DrawText(BambooPlay(WorldInfo.Game).PlayerMoney);
	}
	else if (Nameplate == "Your Time:")
	{
		Canvas.DrawText(BambooPlay(WorldInfo.Game).PlayerTime);
	}
}

//Defaults
defaultproperties
{
	HUDGFx = class'BambooPlay.BambooGFx'
	HUDbegin = class'BambooPlay.BambooGFbegin'
	HUDpause = class'BambooPlay.BambooGFpause'
}