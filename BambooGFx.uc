//GFxPlayer Instance
class BambooGFx extends GFxMoviePlayer;

//Vars
	var WorldInfo ThisWorld;
	var GFxObject MoneyTF, LDashMC, TimeTF, RDashMC;
	var int CurrentMoney, CurrentTime;

//Events
simulated event TickHud( float DeltaTime )
{
	if(CurrentMoney != BambooPlay(ThisWorld.Game).PlayerMoney)
	{
		CurrentMoney = BambooPlay(ThisWorld.Game).PlayerMoney;
		MoneyTF.SetText(CurrentMoney);
	}
	if(CurrentTime != BambooPlay(ThisWorld.Game).PlayerTime)
	{
		CurrentTime = BambooPlay(ThisWorld.Game).PlayerTime;
		TimeTF.SetText(CurrentTime);
	}
}

//Functions
simulated function Init(optional LocalPlayer Slayer)
{
	super.Init(Slayer);
	ThisWorld = GetPC().WorldInfo;
	Start();
	Advance(0);
	LDashMC = GetVariableObject("_root.ldash");
	MoneyTF = GetVariableObject("_root.bmoney");
	RDashMC = GetVariableObject("_root.rdash");
	TimeTF = GetVariableObject("_root.btime");
	ClearSelf(True);
}

simulated function ClearSelf(optional bool IsClear)
{
	if(CurrentMoney != 0)
	{
		MoneyTF.SetString("text","");
		LDashMC.GotoAndStopI(0);
		CurrentMoney = 0;
	}
	if(CurrentTime != 0)
	{
		TimeTF.SetString("text","");
		RDashMC.GotoAndStopI(0);
		CurrentTime = 0;
	}
}

//Defaults
defaultproperties
{
	MovieInfo = SwfMovie'BambooGame.Fla.udk_hud_x'
	bDisplayWithHudOff = False
	bEnableGammaCorrection = False
	bAllowInput = False
	bAllowFocus = False
}
