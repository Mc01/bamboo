class K_SilverCoin extends BambooActor;

defaultproperties
{
	Begin Object Class=StaticMeshComponent Name=SilverCoin
		StaticMesh=StaticMesh'BambooGame.Mx.CoinS'
		
		bCastDynamicShadow=True
		bForceDirectLightMap=False
		bAllowCullDistanceVolume=True
		bAcceptsStaticDecals=False
		bAcceptsDynamicDecals=False
		CastShadow=True
		bAcceptsDynamicDominantLightShadows=True
		bAcceptsLights=True
		bAcceptsDynamicLights=True
		bUsePrecomputedShadows=False
	End Object
	Components.Add(SilverCoin)
	ActorMesh=SilverCoin
	
	Money=-10
}