//Game Handler, initialization of game
class BambooPlay extends GameInfo;

//Vars
	//Player related
	var BambooController MainPlayer;
	var int PlayerMoney, PlayerTime;
	var int PointsAccumulated;
	
	//Pawn related
	var BambooMonkey MainPawn;
	var vector PawnV;
	var rotator PawnR;
	
	//Asset related
	var array<BambooActor> K_List;
	var PrimitiveComponent ChestForActor;
	
	//Game related
	var int DebugMode;
	var string OpenState;
	var int liteFlag; //Pause Screen
	
	//Custom
	var float FlowSpawn, MoveTime, CoreTime; //Times
	var int HeightVector, XGravity; //Distances
	var float SmoothAnim, CoreAnim; //Anims
	var float SleepFloor, SleepMonkey; //Sleeps
	
	//Save System
	var int StoredMoney;

//Events
simulated event PostBeginPlay()
{
    `log("Welcome to BambooGame!");
	WorldInfo.WorldGravityZ = -0.0001;
	
	PawnV=vect(36,608,16);
	PawnR=rot(0,0x4000,0);
	MainPawn = Spawn(class'BambooMonkey',,,PawnV,PawnR,,True);
	
	PawnV=vect(36,128,656);
	PawnR=rot(0,0,0);
	
	LoadScore(); //Load or assign HighestScore
	PlayerTime = 60;
	PlayerMoney = 0;
	K_List.Add(2);
	PointsAccumulated = 0;
	
	liteFlag = 0;
	`log("PostBegin() executed!");
}

simulated event PostLogin( PlayerController NewPlayer )
{
	super.PostLogin(NewPlayer);
	BambooController(NewPlayer).OwnedBamboo = MainPawn;
	MainPlayer = BambooController(NewPlayer);
	MainPawn.SuperEvent();
	ChestForActor = MainPawn.BChest;
	`log("PostLogin() executed!");
}

simulated event Tick(float DeltaTime)
{
	super.Tick(DeltaTime);
	if((MainPlayer!=None) && (MainPlayer.BFlag==1))
	{
		MainPlayer.BFlag=2;
		BambooFlow();
	}	
}

simulated event BambooFlow()
{
	local float BRule;
	if((MainPlayer!=None) && (MainPlayer.BFlag==2))
	{
		BRule = RandRange(0.0,1.0);
		
		if(BRule < 0.5) 
		{
			MainPlayer.BFlag=3;
			PawnV.Z = HeightVector;
			self.SetTimer(FlowSpawn,False,'SpawnSilver');
		}
		else if(BRule >= 0.5)
		{
			MainPlayer.BFlag=3;
			PawnV.Z = HeightVector;
			self.SetTimer(FlowSpawn,False,'SpawnGold');
		}
		
		if(PlayerTime <= 0)
		{
			GotoState('KillThem');
		}
		
		if(PlayerMoney >= (300*PointsAccumulated))
		{
			PointsAccumulated++;
			PlayerTime += (60/PointsAccumulated);
		}
	}
}

//Functions
simulated function SpawnSilver()
{
	if((MainPlayer!=None) && (MainPlayer.BFlag==3))
	{
		SpawnIt(0,class'K_SilverCoin');
	}
}

simulated function SpawnGold()
{
	if((MainPlayer!=None) && (MainPlayer.BFlag==3))
	{
		SpawnIt(1,class'K_GoldCoin');
	}
}

simulated function SpawnIt(int index, class<BambooActor> SpawnClass)
{
	PawnV.Y = 128 + 96 * int(10*RandRange(0.0,1.0));
	K_List[index] = Spawn(SpawnClass,,,PawnV,PawnR,,True);
	MainPlayer.BFlag=1;	
}

simulated function ReadyToPlay()
{
	PlayerTime = 60;
	PlayerMoney = 0;
	MainPlayer.StartCall();
	GotoState('GamePlaying');
}

simulated function ReadyToKill()
{
	local BambooActor UAC;
	MainPlayer.StopCall();
	foreach WorldInfo.AllActors(class'BambooActor', UAC)
    	{
			UAC.Destroy();
		}
	if(PlayerMoney > StoredMoney)
	{
		StoredMoney = PlayerMoney;
		SaveScore();
	}
	GotoState('AfterGame');
}

simulated function SaveScore()
{
	local BambooSave SaveState;
	SaveState = new class'BambooSave';
	
	SaveState.HighScore = PlayerMoney;
	class'Engine'.static.BasicSaveObject(SaveState,"BambooSaved.bin",True,0);
}

simulated function LoadScore()
{
	local BambooSave SaveState;
	SaveState = new class'BambooSave';
	
	if(class'Engine'.static.BasicLoadObject(SaveState,"BambooSaved.bin",True,0))
	{
		StoredMoney = SaveState.HighScore;
	}
	else 
	{
		StoredMoney = 0;
	}
}

//States
auto state PendingMatch
{
	Begin:
	OpenState = "Pending";
	GoToState('Arrived');
}


state Arrived
{
	Begin:
	OpenState = "Arrived";
	liteFlag = 1;
}

state BeforeBegin
{
	Begin:
	OpenState = "BeforeBegin";
	PointsAccumulated = 1;
	liteFlag = 3;
	self.SetTimer(1.0,False,'ReadyToPlay');
}

state GamePlaying
{
	Begin:
	OpenState = "GamePlaying";
}

state KillThem
{
	Begin:
	OpenState = "KillThem";
	self.SetTimer(1.0,False,'ReadyToKill');
}

state AfterGame
{
	Begin:
	OpenState = "AfterGame";
	liteFlag = 1;
}

//Defaults
defaultproperties
{
	DefaultPawnClass=class'BambooPlay.BambooMonkey'
	PlayerControllerClass=class'BambooPlay.BambooController'
	HUDType=class'BambooPlay.BambooHUD'
	
	DebugMode=0
	FlowSpawn = 1.0
	HeightVector = 656
	MoveTime = 3.0
	XGravity = -500
	SleepFloor = 0.1
	SleepMonkey = 0.3
}