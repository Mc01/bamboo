//Pawn Handler, its Players visualization
class BambooMonkey extends Pawn;

//Vars
	var AnimNodeBlendList RootList;
	var BambooController MasterObject;
	var array<BambooController> MasterList;
	var StaticMeshComponent BChest;
	
	//Custom
	var float CoreAnim, CoreTime;

//Events
simulated event PostBeginPlay()
{
	RootList = AnimNodeBlendList(Mesh.FindAnimNode('Root'));
	Mesh.AttachComponentToSocket(BChest, 'HandleSocket');
}

simulated event OnAnimEnd(AnimNodeSequence XNode, float PlayedTime, float ExcessTime)
{
	local vector MonkeyOffset;
	if((MasterList[0].Passport == 1) && (XNode.AnimSeqName == 'Jump0_Init'))
	{
		RootList.StopAnim();
		RootList.SetActiveChild(2, 0.1);
		RootList.PlayAnim(false, CoreAnim, 0.0);
		MonkeyOffset = vect(0,-96,0);
		MasterList[0].MoveVector = MonkeyOffset;
		MasterList[0].StartVector = MasterList[0].StopVector;
		MasterList[0].StopVector = MasterList[0].StartVector + MonkeyOffset;
		MasterList[0].MoveTime = CoreTime;
		MasterList[0].Passport = 3;
	}
	else if ((MasterList[0].Passport == 2) && (XNode.AnimSeqName == 'Jump0_Init'))
	{
		RootList.StopAnim();
		RootList.SetActiveChild(3, 0.1);
		RootList.PlayAnim(false, CoreAnim, 0.0);
		MonkeyOffset = vect(0,96,0);
		MasterList[0].MoveVector = MonkeyOffset;
		MasterList[0].StartVector = MasterList[0].StopVector;
		MasterList[0].StopVector = MasterList[0].StartVector + MonkeyOffset;
		MasterList[0].MoveTime = CoreTime;
		MasterList[0].Passport = 3;
	}
	else if((MasterList[0].Passport == 6) && (XNode.AnimSeqName == 'Jump2_Finish'))
	{
		RootList.StopAnim();
		RootList.SetActiveChild(0, 0.1);
		MasterList[0].Passport = 0;
		MonkeyOffset = vect(0,0,0);
	}
	else 
	{
		if(BambooPlay(WorldInfo.Game).DebugMode == 1){
		`log("It's Name of Master:"$MasterList[0]);
		`log("It's Passport:"$MasterList[0].Passport);
		`log("It's Name:"$XNode.AnimSeqName);
		}
	}
}

//Functions
simulated function SuperEvent()
{
	foreach WorldInfo.AllControllers(class'BambooController', MasterObject) //Mastery
    {
		MasterList.AddItem(MasterObject);
		`log("Master is:"$MasterObject);
    }
	MasterList[0].StartVector = Location;
	MasterList[0].StopVector = MasterList[0].StartVector;
}

//Defualts
defaultproperties
{
	Begin Object Class=DynamicLightEnvironmentComponent Name=MyLightBamboo //Light for Pawn
    	bSynthesizeSHLight=True
    	bIsCharacterLightEnvironment=True //Dedicated for Pawn
	End Object
	Components.Add(MyLightBamboo) //Store Component
	
	Begin Object Class=SkeletalMeshComponent Name=BambooComponent //Skelmesh for Pawn
		SkeletalMesh=SkeletalMesh'BambooGame.Char.My_Monkey0'
		AnimTreeTemplate=AnimTree'BambooGame.Char.Ty_Monkey0'
		AnimSets.Add(AnimSet'BambooGame.Char.Ay_Monkey0')
		PhysicsAsset=PhysicsAsset'BambooGame.Char.Py_Monkey0'
		LightEnvironment=MyLightBamboo //Init of Light
		CollideActors=True
		BlockRigidBody=True
		BlockActors=True
		bAllowCullDistanceVolume=True
		bAcceptsStaticDecals=False
		bAcceptsDynamicDecals=False
		CastShadow=True
		bAcceptsDynamicDominantLightShadows=True
		bAcceptsLights=True
		bAcceptsDynamicLights=True
		bUsePrecomputedShadows=False
	End Object
	Components.Add(BambooComponent)
	Mesh=BambooComponent
	
	Begin Object Class=StaticMeshComponent Name=BambooChest
		StaticMesh=StaticMesh'BambooGame.Mx.Chest0'
		bCastDynamicShadow=True
		CollideActors=True
		BlockRigidBody=True
		BlockActors=True
		bForceDirectLightMap=False
		bAllowCullDistanceVolume=True
		bAcceptsStaticDecals=False
		bAcceptsDynamicDecals=False
		CastShadow=True
		//bCastDynamicShadow=True
		bAcceptsDynamicDominantLightShadows=True
		bAcceptsLights=True
		bAcceptsDynamicLights=True
		bUsePrecomputedShadows=False
	End Object
	Components.Add(BambooChest)
	BChest=BambooChest
	
	bCollideActors=true
	bBlockActors=true
	
	CoreAnim = 1.0
	CoreTime = 0.2
}
