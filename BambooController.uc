//Controller Handler, using proxies enabled control of game pawns
class BambooController extends PlayerController;

//Vars
	var BambooMonkey OwnedBamboo;
	var vector MoveVector, StartVector, StopVector;
	var float MoveTime;
	var int Passport, BFlag, GFlag, MacroGravity;
	var bool GamePlay;
	
	//Custom
	var float SmoothAnim;

//Events
simulated event PostBeginPlay()
{
    `log("BambooController works!");
	super.PostBeginPlay(); //Init of SuperClass
	
	GamePlay = True;
	
	Passport = 0; //Init of Passport
	BFlag=0;
	GFlag=0;
	
	MoveTime = 0;
	MoveVector = vect(0,0,0);
	
	MacroGravity = 0;
}

simulated event PlayerTick(float DeltaTime)
{
	super.PlayerTick(DeltaTime);
	
	if((GamePlay == True) && (GFlag == 0) && (BambooPlay(WorldInfo.Game).OpenState == "GamePlaying"))
	{
		GFlag=1;
		TimeFlow();
	}
	
	if(Passport == 3)
	{
		OwnedBamboo.Move(StartVector - OwnedBamboo.Location);
		Passport = 4;
	}
	if(Passport == 4)
	{
		XMove(MoveVector, MoveTime, DeltaTime);
	}
	if(Passport == 5)
	{
		OwnedBamboo.Move(StopVector - OwnedBamboo.Location);
		OwnedBamboo.RootList.StopAnim();
		OwnedBamboo.RootList.SetActiveChild(4, 0.1);
		OwnedBamboo.RootList.PlayAnim(false, SmoothAnim, 0.0);
		Passport = 6;
	}
}

simulated event TimeFlow()
{
	if((GamePlay == True) && (GFlag == 1))
	{
		GFlag=2;
		self.SetTimer(1.0,False,'XTime');
	}
}

//Functions
simulated function XTime()
{
	if(GFlag == 2)
	{
		BambooPlay(WorldInfo.Game).PlayerTime -= 1;
		GFlag=0;
	}
}

simulated function XMove(vector XVector, float XTime, float DeltaTime)
{
	local vector DeltaVector;
	local float TimeElapsed;
	
	TimeElapsed = XTime / DeltaTime;
	DeltaVector = XVector /  TimeElapsed;
	
	OwnedBamboo.Move(DeltaVector);
	
	MoveVector = MoveVector - DeltaVector;
	MoveTime = MoveTime - DeltaTime;
	
	if((OwnedBamboo.Location == StopVector) || (MoveVector == (vect(0,0,0))) || (MoveTime <= 0))
	{
		if(BambooPlay(WorldInfo.Game).DebugMode == 1){
		`log("StartV: "$StartVector);
		`log("StopV: "$StopVector);
		}
		
		MoveTime = 0;
		MoveVector = vect(0,0,0);
		Passport = 5;
	}
}

exec function EscapeArtist()
{
	ConsoleCommand("Quit");
}

exec function MacroCosmos()
{
	`log(BambooPlay(WorldInfo.Game).GetStateName());
	if((BambooPlay(WorldInfo.Game).OpenState == "Arrived") || (BambooPlay(WorldInfo.Game).OpenState == "AfterGame"))
	{
		BambooPlay(WorldInfo.Game).GotoState('BeforeBegin');
	}
	else if((GamePlay == True) && (MacroGravity == 0) && (BambooPlay(WorldInfo.Game).OpenState == "GamePlaying"))
	{
		MacroGravity = 1;
		BambooPlay(WorldInfo.Game).liteFlag = 2;
		SetPause(True);
	}
	else if((GamePlay == True) && (MacroGravity == 1) && (BambooPlay(WorldInfo.Game).OpenState == "GamePlaying"))
	{
		MacroGravity = 0;
		BambooPlay(WorldInfo.Game).liteFlag = 3;
		SetPause(False);
	}
}

function StartCall()
{
		BFlag=1;
}

function StopCall()
{
		BFlag=0;
}

exec function change3()
{
	local ActorComponent UAC;
	if(BambooPlay(WorldInfo.Game).DebugMode == 1)
	{
		`log("Pawn: "$OwnedBamboo);
		`log("Passport: "$Passport);
		`log("Vector: "$MoveVector);
		`log("Time: "$MoveTime);
		`log("Location: "$OwnedBamboo.Location);
		foreach OwnedBamboo.AllOwnedComponents(class'ActorComponent', UAC)
    		{
				`log(UAC);
			}
	}
}

exec function Ais0()
{
	if ((OwnedBamboo != None) && (Passport == 0) && (BambooPlay(WorldInfo.Game).OpenState == "GamePlaying"))
	{
		if(OwnedBamboo.Location.Y <= 128)
		{
			`log("A is Holding!");
			OwnedBamboo.RootList.StopAnim();
			OwnedBamboo.Move((vect(36,128,16))- OwnedBamboo.Location);
				
		}
		else
		{		
			Passport = 1;
			`log("A is Left!");
			OwnedBamboo.RootList.StopAnim();
			OwnedBamboo.RootList.SetActiveChild(1, 0.1);
			OwnedBamboo.RootList.PlayAnim(false, SmoothAnim, 0.0);
		}
	}
}

exec function Dis2()
{
	if ((OwnedBamboo != None) && (Passport == 0) && (BambooPlay(WorldInfo.Game).OpenState == "GamePlaying"))
	{
		if(OwnedBamboo.Location.Y >= 1088)
		{
			`log("D is Holding!");
			OwnedBamboo.RootList.StopAnim();
			OwnedBamboo.Move((vect(36,1088,16))- OwnedBamboo.Location);		
		}
		else
		{	
			Passport = 2;
			`log("D is Right!");
			OwnedBamboo.RootList.StopAnim();
			OwnedBamboo.RootList.SetActiveChild(1, 0.1);
			OwnedBamboo.RootList.PlayAnim(false, SmoothAnim, 0.0);
		}
	}
}

//Defaults
defaultproperties
{
	SmoothAnim = 1.0
}