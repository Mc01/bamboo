//GFxPlayer Instance
class BambooGFlite extends GFxMoviePlayer;

//Vars
	var WorldInfo ThisWorld;
	var GFxObject SpaceMC;

//Functions
simulated function Init(optional LocalPlayer Slayer)
{
	super.Init(Slayer);
	ThisWorld = GetPC().WorldInfo;
	Start();
	Advance(0);
	SpaceMC = GetVariableObject("_root.space");
}

//Defaults
defaultproperties
{
	MovieInfo = SwfMovie'BambooGame.Fla.udk_hud_b'
	bDisplayWithHudOff = False
	bEnableGammaCorrection = False
	bAllowInput = False
	bAllowFocus = False
}
